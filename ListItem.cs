﻿namespace OBrIMACAD
{
    public class ListItem
    {
        public string Name;
        public string Value;
        public ListItem(string name, string value)
        {
            Name = name; Value = value;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}