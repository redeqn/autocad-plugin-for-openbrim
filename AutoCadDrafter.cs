﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.AutoCAD;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
using OBrIM;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using System.Windows.Input;

namespace OBrIMACAD
{
    public static class AutoCadDrafter
    {
        public static Point3d DrawPoint(OBrIMObj Object)
        {

            double X = 0;
            double Y = 0;
            double Z = 0;
            try
            {
                X = Object.GetParam("X").Value.AsNumber();
                Y = Object.GetParam("Y").Value.AsNumber();
                Z = Object.GetParam("Z").Value.AsNumber();
            }
            catch (Exception)
            {
            }
            Point3d P = new Point3d(X, Y, Z);
            return P;
        }
        public static void DrawLine(OBrIMObj Object)
        {
            OBrIMObj PointObjectStart = Object.Objects.Get(0);
            OBrIMObj PointObjectEnd = Object.Objects.Get(1);

            Point3d PointStart = DrawPoint(PointObjectStart);
            Point3d PointEnd = DrawPoint(PointObjectEnd);

            Line Newline = new Line(PointStart, PointEnd);

            double Xpos = Object.GetParam("X").Value.AsNumber();
            double Ypos = Object.GetParam("Y").Value.AsNumber();
            double RZ = Object.GetParam("RZ").Value.AsNumber();
            Point3d Pos = new Point3d(Xpos, Ypos, RZ);
            DrawInModel(Newline, Pos);

            //MoveObject(Newline,Pos);

        }

        private static void DrawInModel(Entity Entity, Point3d position)
        {
            Document Doc = Application.DocumentManager.MdiActiveDocument;
            Editor Ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database DataBase = Ed.Document.Database;
            using (Transaction Trans = DataBase.TransactionManager.StartTransaction())
            {
                Doc.LockDocument();
                BlockTableRecord BlockTableRecord = (BlockTableRecord)Trans.GetObject(DataBase.CurrentSpaceId, OpenMode.ForWrite);
                BlockTableRecord.AppendEntity(Entity);


                Point3d Origin = new Point3d(0, 0, 0);
                Vector3d Displacment = Origin.GetVectorTo(position);
                Matrix3d Transformation = Matrix3d.Displacement(Displacment);
                Entity.TransformBy(Transformation);

                Trans.AddNewlyCreatedDBObject(Entity, true);

                Trans.Commit();
            }
        }

        private static void MoveObject(Line newline, Point3d Position)
        {
            Document Doc = Application.DocumentManager.MdiActiveDocument;
            Editor Ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database DataBase = Ed.Document.Database;
            using (Transaction Trans = DataBase.TransactionManager.StartTransaction())
            {
                Doc.LockDocument();

                BlockTable BlockTable = Trans.GetObject(DataBase.BlockTableId, OpenMode.ForRead) as BlockTable;
                // Open the Block table record Model space for write
                BlockTableRecord BlockTableRecord = Trans.GetObject(BlockTable[BlockTableRecord.ModelSpace], OpenMode.ForWrite) as BlockTableRecord;
                //BlockTableRecord BlockTableRecord = (BlockTableRecord)Trans.GetObject(DataBase.CurrentSpaceId, OpenMode.ForWrite);

                Point3d Origin = new Point3d(0, 0, 0);
                Vector3d Displacment = Origin.GetVectorTo(Position);
                Matrix3d Transformation = Matrix3d.Displacement(Displacment);
                newline.TransformBy(Transformation);
                Trans.Commit();
            }
        }

        public static void DrawCadShape(OBrIMObj Object)
        {
            Polyline FreeLine = new Polyline();

            FreeLine.Closed = true;
            OBrIMObjList PointList = Object.Objects;
            double Xpos = 0;
            double Ypos = 0;
            double RZ = 0;
            for (int i = 0; i < PointList.Count; i++)
            {
                OBrIMObj Point = PointList.Get(i);
                try
                {
                    double X = Point.GetParam("X").Value.AsNumber();
                    double Y = Point.GetParam("Y").Value.AsNumber();
                    Point2d Vertex = new Point2d(X, Y);
                    FreeLine.AddVertexAt(i, Vertex, 0, 0, 0);

                    Xpos = Object.GetParam("X").Value.AsNumber();
                    Ypos = Object.GetParam("Y").Value.AsNumber();
                    RZ = Object.GetParam("RZ").Value.AsNumber();
                }
                catch (Exception)
                {
                }
            }

            Point3d Pos = new Point3d(Xpos, Ypos, RZ);
            DrawInModel(FreeLine, Pos);
        }
        public static void DrawCircle(OBrIMObj Object)
        {
            try
            {
                double X = Object.GetParam("X").Value.AsNumber();
                double Y = Object.GetParam("Y").Value.AsNumber();
                double RZ = Object.GetParam("RZ").Value.AsNumber();
                double Radius = Object.GetParam("Radius").Value.AsNumber();
                Point3d P = new Point3d(X, Y, RZ);
                Circle newCircle = new Circle(P, Vector3d.ZAxis, Radius);
                DrawInModel(newCircle);
            }
            catch (Exception)
            {
            }
        }
        public static void DrawDimention(OBrIMObj Object)
        {
            RotatedDimension acRotDim = new RotatedDimension();
            acRotDim.SetDatabaseDefaults();
            acRotDim.XLine1Point = new Point3d(0, 0, 0);
            acRotDim.XLine2Point = new Point3d(6, 3, 0);
            acRotDim.Rotation = 0.707;
            acRotDim.DimLinePoint = new Point3d(0, 5, 0);

            DrawInModel(acRotDim);
        }
        public static void DrawContainer()
        {
        }
        public static void DrawInModel(Entity Entity)
        {
            Document Doc = Application.DocumentManager.MdiActiveDocument;
            Editor Ed = Application.DocumentManager.MdiActiveDocument.Editor;
            Database DataBase = Ed.Document.Database;
            using (Transaction Trans = DataBase.TransactionManager.StartTransaction())
            {
                Doc.LockDocument();
                BlockTableRecord BlockTableRecord = (BlockTableRecord)Trans.GetObject(DataBase.CurrentSpaceId, OpenMode.ForWrite);
                BlockTableRecord.AppendEntity(Entity);
                Trans.AddNewlyCreatedDBObject(Entity, true);
                Trans.Commit();
            }
        }
        public static void DrawFreeLine(OBrIMObj Object)
        {
            OBrIMObjList Points = Object.Objects;
            //List<Point3d> ACADPointList = new List<Point3d>();
            Polyline Freeline = new Polyline();

            for (int i = 0; i < Points.Count; i++)
            {
                OBrIMObj CurrentPoint = Points.Get(i);
                Point3d ACADPoint = DrawPoint(CurrentPoint);
                Point2d AcadPoint2d = new Point2d(ACADPoint.X, ACADPoint.Y);
                Freeline.AddVertexAt(i, AcadPoint2d, 0, 0, 0);
            }

            //Line Newline = new Line(PointStart, PointEnd);

            double Xpos = Object.GetParam("X").Value.AsNumber();
            double Ypos = Object.GetParam("Y").Value.AsNumber();
            double RZ = Object.GetParam("RZ").Value.AsNumber();
            Point3d Pos = new Point3d(Xpos, Ypos, RZ);

            DrawInModel(Freeline, Pos);
        }
        public static void DrawCad(OBrIMObj Object)
        {
            //Most Objects are dont Except for Arcs -  Curve - Brezer - Dimentions 
            String Type = Object.ObjType;

            switch (Type)
            {
                case "CADDShape":
                    AutoCadDrafter.DrawCadShape(Object);
                    break;
                case "CADDLine":
                    AutoCadDrafter.DrawLine(Object);
                    break;
                case "CADDDimensionLine":
                    AutoCadDrafter.DrawDimention(Object);
                    break;
                case "CADDContainer":
                    AutoCadDrafter.DrawContainer();
                    break;
                case "CADDCircle":
                    AutoCadDrafter.DrawCircle(Object);
                    break;
                case "CADDFreeline":
                    AutoCadDrafter.DrawFreeLine(Object);
                    break;
                case "CADDRect":
                    AutoCadDrafter.DrawRectangle(Object);
                    break;
                case "CADDEllipse":
                    AutoCadDrafter.DrawEllipse(Object);
                    break;
                case "CADDArc":
                    AutoCadDrafter.DrawArc(Object);
                    break;
                case "CADDBezier":
                    AutoCadDrafter.DrawArc(Object);
                    break;
                case "CADDDimensionRadius":
                    AutoCadDrafter.DrawDimentionRadius(Object);
                    break;
                case "CADDText":
                    AutoCadDrafter.DrawText(Object);
                    break;
                case "CADDCloud":
                    AutoCadDrafter.CreateRevisionCloud(Object);
                    break;
                default:
                    Type = "" + Type;
                    break;
            }
        }

        private static void CreateRevisionCloud(OBrIMObj Object)
        {
        }

        private static void DrawText(OBrIMObj Object)
        {
        }

        private static void DrawDimentionRadius(OBrIMObj Object)
        {
        }

        private static void DrawArc(OBrIMObj Object)
        {
            try
            {
                double X = Object.GetParam("X").Value.AsNumber();
                double Y = Object.GetParam("Y").Value.AsNumber();
                double RZ = Object.GetParam("RZ").Value.AsNumber();
                Point2d P = new Point2d(X, Y);

                OBrIMObjList Points = Object.Objects;

                Point3d[] ACADPointList = new Point3d[Points.Count];

                for (int i = 0; i < Points.Count; i++)
                {
                    OBrIMObj OBrimPoint = Points.Get(i);
                    Point3d Point = DrawPoint(OBrimPoint);
                    ACADPointList[i] = Point;
                }
                Point3dCollection Collection = new Point3dCollection(ACADPointList);
                Spline NewArc = new Spline(Collection, 9, 0.01);
                DrawInModel(NewArc);

            }
            catch (Exception)
            {
            }
        }

        private static void DrawEllipse(OBrIMObj Object)
        {
            //known Bug 
            //You need to Set the Major and Minor Radius on the Ellipse 
            //It has a Center and a Ratio but not a Radius 

            try
            {
                double X = Object.GetParam("X").Value.AsNumber();
                double Y = Object.GetParam("Y").Value.AsNumber();
                double RZ = Object.GetParam("RZ").Value.AsNumber();
                double Radius1 = Object.GetParam("Radius1").Value.AsNumber();
                double Radius2 = Object.GetParam("Radius2").Value.AsNumber();
                Point3d P = new Point3d(X, Y, RZ);
                Radius1 = Math.Abs(Radius1);
                Radius2 = Math.Abs(Radius2);

                if (Radius1 > Radius2)
                {
                    Ellipse NewEllipse = new Ellipse(P, Vector3d.ZAxis, (Vector3d.XAxis * Radius1), (Radius2 / Radius1), 0, (360 * Math.PI / 180));
                    DrawInModel(NewEllipse);

                    //NewEllipse.StartPoint = new Point3d(NewEllipse.Center.X + Radius1/2 , NewEllipse.Center.Y  , NewEllipse.Center.Z);
                    //NewEllipse.EndPoint = new Point3d(NewEllipse.Center.X - Radius1 / 2, NewEllipse.Center.Y, NewEllipse.Center.Z);

                }
                else if (Radius2 > Radius1)
                {
                    Ellipse NewEllipse = new Ellipse(P, Vector3d.ZAxis, (Vector3d.YAxis * Radius2), (Radius1 / Radius2), 0, (360 * Math.PI / 180));

                    DrawInModel(NewEllipse);

                    //NewEllipse.StartPoint = new Point3d(NewEllipse.Center.X + Radius2 / 2, NewEllipse.Center.Y, NewEllipse.Center.Z);
                    //NewEllipse.EndPoint = new Point3d(NewEllipse.Center.X - Radius2 / 2, NewEllipse.Center.Y, NewEllipse.Center.Z);
                }
            }
            catch (Exception)
            {
            }
        }
        private static void DrawRectangle(OBrIMObj Object)
        {
            try
            {
                double X = Object.GetParam("X").Value.AsNumber();
                double Y = Object.GetParam("Y").Value.AsNumber();
                double RZ = Object.GetParam("RZ").Value.AsNumber();
                double W = Object.GetParam("W").Value.AsNumber();
                double H = Object.GetParam("H").Value.AsNumber();

                Point2d P = new Point2d(X - W / 2, Y - H / 2);
                Point2d P2 = new Point2d(X + W / 2, Y - H / 2);
                Point2d P3 = new Point2d(X + W / 2, Y + H / 2);
                Point2d P4 = new Point2d(X - W / 2, Y + H / 2);

                Polyline Rectangle = new Polyline(4);
                Rectangle.Closed = true;
                Rectangle.AddVertexAt(0, P, 0, 0, 0);
                Rectangle.AddVertexAt(0, P2, 0, 0, 0);
                Rectangle.AddVertexAt(0, P3, 0, 0, 0);
                Rectangle.AddVertexAt(0, P4, 0, 0, 0);
                //Rectangle3d Rectangle = new Rectangle3d(P, P2, P3, P4);

                DrawInModel(Rectangle);
            }
            catch (Exception)
            {
            }
        }

        public static void RetreiveParam()
        {

        }
    }
}
