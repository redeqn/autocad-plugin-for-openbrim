﻿using Autodesk.AutoCAD.Runtime;
using Autodesk.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autodesk.AutoCAD;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Reflection;
using System.IO;

[assembly: ExtensionApplication(typeof(OBrIMACAD.Command))]
//First Loaded Class: 
//Loads OpenBrim , TAB , Button 
//Change Button Sizes , Resource Image , TAB Sizes , ADD Buttons and ComboBoxes => TO Be Done 

namespace OBrIMACAD
{
    class Command : IExtensionApplication
    {
        public void Initialize()
        {
            OpenBrim();
        }
        public void OpenBrim()
        {
            RibbonControl OpenBrimRibbon = Autodesk.Windows.ComponentManager.Ribbon;


            RibbonTab OpenBrimTab = new RibbonTab()
            {
                //Change This to change the Starting TAB Name
                Title = "OpenBrIM",
                Id = "OpenBrim_APP_ID"
            };
            OpenBrimRibbon.Tabs.Add(OpenBrimTab);
            OpenBrimTab.IsActive = true;

            Autodesk.Windows.RibbonPanelSource PanelSource = new RibbonPanelSource()
            {
                Title = "OpenBrIM"
            };

            RibbonPanel OpenBrimPanel = new RibbonPanel()
            {
                Source = PanelSource
            };
            OpenBrimTab.Panels.Add(OpenBrimPanel);
            OpenBrimPanel.ChangeImageSizePriority = 1;

            // the image is located inside media folder, we will use the logo that is for dark background
            var ImageLocation = Utils.ApplicationPath + "\\Media\\obrimD32x32.png";

            RibbonButton OpenBrimInitButton = new RibbonButton()
            {
                Name = "InitForm",
                // Text = "StartApp",
                CommandParameter = "OpenBrIMForm",
                ShowText = true,
                Size = RibbonItemSize.Large,
				LargeImage = LoadButtonImage(ImageLocation),
                ShowImage = true,
                ResizeStyle = RibbonItemResizeStyles.ChangeSize,
                CommandHandler = new CommandHandler()
            };
            RibbonToolTip ButtonToolTip = new RibbonToolTip()
            {
                //Changes ToolTip And Adds Description To it 
                Title = "Initializes OpenBrIM Platform",
                Content = "Starts OpenBrIM Platform in Autocad",
                ExpandedContent = "Navigate through OpenBrIM content and update cloud model using AutoCAD"
            };
            OpenBrimInitButton.ToolTip = ButtonToolTip;
            PanelSource.Items.Add(OpenBrimInitButton);
            OpenBrimTab.IsActive = true;
        }
        private ImageSource LoadButtonImage(string ImageLocation)
        {
            BitmapImage Image = new BitmapImage();
            Image.BeginInit();
            Image.UriSource = new Uri(ImageLocation);
            //Image.UriSource = new Uri(string.Format("pack://application:,,,/{0};component/{1}", Assembly.GetExecutingAssembly().GetName().Name, ImageLocation));
            Image.EndInit();
            return Image;
         }
        [CommandMethod("OpenBrIMForm")]
        public static void ShowForm()
        {
            MainForm OpenBrIMForm = new MainForm();
            OpenBrIMForm.Show();
        }
        public void Terminate()
        {
        }
    }
    public class CommandHandler : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            RibbonButton Button = parameter as RibbonButton;
            if (Button.Name == "InitForm")
            {
                Command.ShowForm();
            }
        }
    }


}
