﻿namespace OBrIMACAD
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Tab_Control = new System.Windows.Forms.TabControl();
            this.Tab_Project = new System.Windows.Forms.TabPage();
            this.lst_objects = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.pnl_3D = new System.Windows.Forms.Panel();
            this.lst_Author = new System.Windows.Forms.ComboBox();
            this.lst_proj = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Tab_CAD = new System.Windows.Forms.TabPage();
            this.pnl_SelEntites = new System.Windows.Forms.Panel();
            this.lbl_EllipseNo = new System.Windows.Forms.Label();
            this.lbl_Ellipse = new System.Windows.Forms.Label();
            this.lst_EntADD = new System.Windows.Forms.ComboBox();
            this.btn_EntDrwADD = new System.Windows.Forms.Button();
            this.lbl_CADShapeNo = new System.Windows.Forms.Label();
            this.lbl_CircleNo = new System.Windows.Forms.Label();
            this.lbl_LineNo = new System.Windows.Forms.Label();
            this.lbl_CADShape = new System.Windows.Forms.Label();
            this.lbl_Circles = new System.Windows.Forms.Label();
            this.lbl_line = new System.Windows.Forms.Label();
            this.lbl_SelEnt = new System.Windows.Forms.Label();
            this.pnl_del = new System.Windows.Forms.Panel();
            this.lst_delDraw = new System.Windows.Forms.ComboBox();
            this.lbl_del = new System.Windows.Forms.Label();
            this.btn_delDrawing = new System.Windows.Forms.Button();
            this.pnl_ADD = new System.Windows.Forms.Panel();
            this.lbl_AddDraw = new System.Windows.Forms.Label();
            this.btn_ADDDraw = new System.Windows.Forms.Button();
            this.txt_DrawingName = new System.Windows.Forms.TextBox();
            this.pnl_Select = new System.Windows.Forms.Panel();
            this.lbl_selDraw = new System.Windows.Forms.Label();
            this.chk_All = new System.Windows.Forms.CheckBox();
            this.chk_Single = new System.Windows.Forms.CheckBox();
            this.lbl_selDrawin = new System.Windows.Forms.Label();
            this.lbl_No = new System.Windows.Forms.Label();
            this.lbl_Count = new System.Windows.Forms.Label();
            this.btn_SelectDrawing = new System.Windows.Forms.Button();
            this.btn_DelDrw = new System.Windows.Forms.Button();
            this.btn_AddCad = new System.Windows.Forms.Button();
            this.btn_Update = new System.Windows.Forms.Button();
            this.btn_Genrate = new System.Windows.Forms.Button();
            this.pnl_CAD = new System.Windows.Forms.Panel();
            this.lbl_Rectangle = new System.Windows.Forms.Label();
            this.lbl_RectangleNo = new System.Windows.Forms.Label();
            this.Tab_Control.SuspendLayout();
            this.Tab_Project.SuspendLayout();
            this.Tab_CAD.SuspendLayout();
            this.pnl_SelEntites.SuspendLayout();
            this.pnl_del.SuspendLayout();
            this.pnl_ADD.SuspendLayout();
            this.pnl_Select.SuspendLayout();
            this.SuspendLayout();
            // 
            // Tab_Control
            // 
            this.Tab_Control.Controls.Add(this.Tab_Project);
            this.Tab_Control.Controls.Add(this.Tab_CAD);
            this.Tab_Control.Location = new System.Drawing.Point(2, 3);
            this.Tab_Control.Name = "Tab_Control";
            this.Tab_Control.SelectedIndex = 0;
            this.Tab_Control.Size = new System.Drawing.Size(650, 574);
            this.Tab_Control.TabIndex = 14;
            this.Tab_Control.SelectedIndexChanged += new System.EventHandler(this.Tab_Objects_SelectedIndexChanged);
            // 
            // Tab_Project
            // 
            this.Tab_Project.Controls.Add(this.lst_objects);
            this.Tab_Project.Controls.Add(this.label2);
            this.Tab_Project.Controls.Add(this.pnl_3D);
            this.Tab_Project.Controls.Add(this.lst_Author);
            this.Tab_Project.Controls.Add(this.lst_proj);
            this.Tab_Project.Controls.Add(this.label1);
            this.Tab_Project.Location = new System.Drawing.Point(4, 22);
            this.Tab_Project.Name = "Tab_Project";
            this.Tab_Project.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_Project.Size = new System.Drawing.Size(642, 548);
            this.Tab_Project.TabIndex = 0;
            this.Tab_Project.Text = "Project";
            this.Tab_Project.UseVisualStyleBackColor = true;
            // 
            // lst_objects
            // 
            this.lst_objects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lst_objects.FormattingEnabled = true;
            this.lst_objects.Location = new System.Drawing.Point(12, 97);
            this.lst_objects.Margin = new System.Windows.Forms.Padding(4);
            this.lst_objects.Name = "lst_objects";
            this.lst_objects.Size = new System.Drawing.Size(135, 21);
            this.lst_objects.TabIndex = 17;
            this.lst_objects.SelectedIndexChanged += new System.EventHandler(this.Lst_objects_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(169, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "3D View";
            // 
            // pnl_3D
            // 
            this.pnl_3D.Location = new System.Drawing.Point(169, 49);
            this.pnl_3D.Name = "pnl_3D";
            this.pnl_3D.Size = new System.Drawing.Size(451, 496);
            this.pnl_3D.TabIndex = 13;
            // 
            // lst_Author
            // 
            this.lst_Author.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lst_Author.FormattingEnabled = true;
            this.lst_Author.Location = new System.Drawing.Point(12, 69);
            this.lst_Author.Margin = new System.Windows.Forms.Padding(4);
            this.lst_Author.Name = "lst_Author";
            this.lst_Author.Size = new System.Drawing.Size(135, 21);
            this.lst_Author.TabIndex = 12;
            this.lst_Author.SelectedIndexChanged += new System.EventHandler(this.Lst_Authors_SelectedIndexChanged);
            // 
            // lst_proj
            // 
            this.lst_proj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.lst_proj.FormattingEnabled = true;
            this.lst_proj.Location = new System.Drawing.Point(12, 40);
            this.lst_proj.Margin = new System.Windows.Forms.Padding(4);
            this.lst_proj.Name = "lst_proj";
            this.lst_proj.Size = new System.Drawing.Size(135, 21);
            this.lst_proj.TabIndex = 6;
            this.lst_proj.SelectedIndexChanged += new System.EventHandler(this.Lst_proj_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Projects";
            // 
            // Tab_CAD
            // 
            this.Tab_CAD.Controls.Add(this.pnl_SelEntites);
            this.Tab_CAD.Controls.Add(this.pnl_del);
            this.Tab_CAD.Controls.Add(this.pnl_ADD);
            this.Tab_CAD.Controls.Add(this.pnl_Select);
            this.Tab_CAD.Controls.Add(this.btn_DelDrw);
            this.Tab_CAD.Controls.Add(this.btn_AddCad);
            this.Tab_CAD.Controls.Add(this.btn_Update);
            this.Tab_CAD.Controls.Add(this.btn_Genrate);
            this.Tab_CAD.Controls.Add(this.pnl_CAD);
            this.Tab_CAD.Location = new System.Drawing.Point(4, 22);
            this.Tab_CAD.Name = "Tab_CAD";
            this.Tab_CAD.Padding = new System.Windows.Forms.Padding(3);
            this.Tab_CAD.Size = new System.Drawing.Size(642, 548);
            this.Tab_CAD.TabIndex = 1;
            this.Tab_CAD.Text = "CAD";
            this.Tab_CAD.UseVisualStyleBackColor = true;
            // 
            // pnl_SelEntites
            // 
            this.pnl_SelEntites.Controls.Add(this.lbl_RectangleNo);
            this.pnl_SelEntites.Controls.Add(this.lbl_Rectangle);
            this.pnl_SelEntites.Controls.Add(this.lbl_EllipseNo);
            this.pnl_SelEntites.Controls.Add(this.lbl_Ellipse);
            this.pnl_SelEntites.Controls.Add(this.lst_EntADD);
            this.pnl_SelEntites.Controls.Add(this.btn_EntDrwADD);
            this.pnl_SelEntites.Controls.Add(this.lbl_CADShapeNo);
            this.pnl_SelEntites.Controls.Add(this.lbl_CircleNo);
            this.pnl_SelEntites.Controls.Add(this.lbl_LineNo);
            this.pnl_SelEntites.Controls.Add(this.lbl_CADShape);
            this.pnl_SelEntites.Controls.Add(this.lbl_Circles);
            this.pnl_SelEntites.Controls.Add(this.lbl_line);
            this.pnl_SelEntites.Controls.Add(this.lbl_SelEnt);
            this.pnl_SelEntites.Enabled = false;
            this.pnl_SelEntites.Location = new System.Drawing.Point(12, 315);
            this.pnl_SelEntites.Name = "pnl_SelEntites";
            this.pnl_SelEntites.Size = new System.Drawing.Size(125, 227);
            this.pnl_SelEntites.TabIndex = 16;
            this.pnl_SelEntites.Visible = false;
            // 
            // lbl_EllipseNo
            // 
            this.lbl_EllipseNo.AutoSize = true;
            this.lbl_EllipseNo.Location = new System.Drawing.Point(82, 142);
            this.lbl_EllipseNo.Name = "lbl_EllipseNo";
            this.lbl_EllipseNo.Size = new System.Drawing.Size(10, 13);
            this.lbl_EllipseNo.TabIndex = 22;
            this.lbl_EllipseNo.Text = "-";
            // 
            // lbl_Ellipse
            // 
            this.lbl_Ellipse.AutoSize = true;
            this.lbl_Ellipse.Location = new System.Drawing.Point(8, 142);
            this.lbl_Ellipse.Name = "lbl_Ellipse";
            this.lbl_Ellipse.Size = new System.Drawing.Size(37, 13);
            this.lbl_Ellipse.TabIndex = 21;
            this.lbl_Ellipse.Text = "Ellipse";
            // 
            // lst_EntADD
            // 
            this.lst_EntADD.FormattingEnabled = true;
            this.lst_EntADD.Location = new System.Drawing.Point(7, 29);
            this.lst_EntADD.Name = "lst_EntADD";
            this.lst_EntADD.Size = new System.Drawing.Size(110, 21);
            this.lst_EntADD.TabIndex = 0;
            // 
            // btn_EntDrwADD
            // 
            this.btn_EntDrwADD.Location = new System.Drawing.Point(7, 56);
            this.btn_EntDrwADD.Name = "btn_EntDrwADD";
            this.btn_EntDrwADD.Size = new System.Drawing.Size(110, 23);
            this.btn_EntDrwADD.TabIndex = 18;
            this.btn_EntDrwADD.Text = "Add to Drawing";
            this.btn_EntDrwADD.UseVisualStyleBackColor = true;
            this.btn_EntDrwADD.Click += new System.EventHandler(this.btn_EntDrwADD_Click);
            // 
            // lbl_CADShapeNo
            // 
            this.lbl_CADShapeNo.AutoSize = true;
            this.lbl_CADShapeNo.Location = new System.Drawing.Point(82, 123);
            this.lbl_CADShapeNo.Name = "lbl_CADShapeNo";
            this.lbl_CADShapeNo.Size = new System.Drawing.Size(10, 13);
            this.lbl_CADShapeNo.TabIndex = 20;
            this.lbl_CADShapeNo.Text = "-";
            // 
            // lbl_CircleNo
            // 
            this.lbl_CircleNo.AutoSize = true;
            this.lbl_CircleNo.Location = new System.Drawing.Point(66, 103);
            this.lbl_CircleNo.Name = "lbl_CircleNo";
            this.lbl_CircleNo.Size = new System.Drawing.Size(10, 13);
            this.lbl_CircleNo.TabIndex = 19;
            this.lbl_CircleNo.Text = "-";
            // 
            // lbl_LineNo
            // 
            this.lbl_LineNo.AutoSize = true;
            this.lbl_LineNo.Location = new System.Drawing.Point(66, 84);
            this.lbl_LineNo.Name = "lbl_LineNo";
            this.lbl_LineNo.Size = new System.Drawing.Size(10, 13);
            this.lbl_LineNo.TabIndex = 16;
            this.lbl_LineNo.Text = "-";
            this.lbl_LineNo.Click += new System.EventHandler(this.lbl_LineNo_Click);
            // 
            // lbl_CADShape
            // 
            this.lbl_CADShape.AutoSize = true;
            this.lbl_CADShape.Location = new System.Drawing.Point(8, 123);
            this.lbl_CADShape.Name = "lbl_CADShape";
            this.lbl_CADShape.Size = new System.Drawing.Size(68, 13);
            this.lbl_CADShape.TabIndex = 18;
            this.lbl_CADShape.Text = "CAD Shapes";
            // 
            // lbl_Circles
            // 
            this.lbl_Circles.AutoSize = true;
            this.lbl_Circles.Location = new System.Drawing.Point(8, 103);
            this.lbl_Circles.Name = "lbl_Circles";
            this.lbl_Circles.Size = new System.Drawing.Size(38, 13);
            this.lbl_Circles.TabIndex = 17;
            this.lbl_Circles.Text = "Circles";
            // 
            // lbl_line
            // 
            this.lbl_line.AutoSize = true;
            this.lbl_line.Location = new System.Drawing.Point(8, 84);
            this.lbl_line.Name = "lbl_line";
            this.lbl_line.Size = new System.Drawing.Size(32, 13);
            this.lbl_line.TabIndex = 16;
            this.lbl_line.Text = "Lines";
            // 
            // lbl_SelEnt
            // 
            this.lbl_SelEnt.AutoSize = true;
            this.lbl_SelEnt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_SelEnt.Location = new System.Drawing.Point(4, 12);
            this.lbl_SelEnt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_SelEnt.Name = "lbl_SelEnt";
            this.lbl_SelEnt.Size = new System.Drawing.Size(86, 13);
            this.lbl_SelEnt.TabIndex = 8;
            this.lbl_SelEnt.Text = "Selected Entities";
            // 
            // pnl_del
            // 
            this.pnl_del.Controls.Add(this.lst_delDraw);
            this.pnl_del.Controls.Add(this.lbl_del);
            this.pnl_del.Controls.Add(this.btn_delDrawing);
            this.pnl_del.Enabled = false;
            this.pnl_del.Location = new System.Drawing.Point(12, 89);
            this.pnl_del.Name = "pnl_del";
            this.pnl_del.Size = new System.Drawing.Size(125, 87);
            this.pnl_del.TabIndex = 17;
            this.pnl_del.Visible = false;
            // 
            // lst_delDraw
            // 
            this.lst_delDraw.FormattingEnabled = true;
            this.lst_delDraw.Location = new System.Drawing.Point(7, 29);
            this.lst_delDraw.Name = "lst_delDraw";
            this.lst_delDraw.Size = new System.Drawing.Size(110, 21);
            this.lst_delDraw.TabIndex = 17;
            this.lst_delDraw.SelectedIndexChanged += new System.EventHandler(this.lst_delDraw_SelectedIndexChanged);
            // 
            // lbl_del
            // 
            this.lbl_del.AutoSize = true;
            this.lbl_del.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_del.Location = new System.Drawing.Point(7, 10);
            this.lbl_del.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_del.Name = "lbl_del";
            this.lbl_del.Size = new System.Drawing.Size(80, 13);
            this.lbl_del.TabIndex = 16;
            this.lbl_del.Text = "Delete Drawing";
            // 
            // btn_delDrawing
            // 
            this.btn_delDrawing.Location = new System.Drawing.Point(7, 56);
            this.btn_delDrawing.Name = "btn_delDrawing";
            this.btn_delDrawing.Size = new System.Drawing.Size(110, 23);
            this.btn_delDrawing.TabIndex = 16;
            this.btn_delDrawing.Text = "Delete";
            this.btn_delDrawing.UseVisualStyleBackColor = true;
            this.btn_delDrawing.Click += new System.EventHandler(this.btn_delDrawing_Click);
            // 
            // pnl_ADD
            // 
            this.pnl_ADD.Controls.Add(this.lbl_AddDraw);
            this.pnl_ADD.Controls.Add(this.btn_ADDDraw);
            this.pnl_ADD.Controls.Add(this.txt_DrawingName);
            this.pnl_ADD.Enabled = false;
            this.pnl_ADD.Location = new System.Drawing.Point(12, 7);
            this.pnl_ADD.Name = "pnl_ADD";
            this.pnl_ADD.Size = new System.Drawing.Size(125, 87);
            this.pnl_ADD.TabIndex = 0;
            this.pnl_ADD.Visible = false;
            // 
            // lbl_AddDraw
            // 
            this.lbl_AddDraw.AutoSize = true;
            this.lbl_AddDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_AddDraw.Location = new System.Drawing.Point(7, 10);
            this.lbl_AddDraw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_AddDraw.Name = "lbl_AddDraw";
            this.lbl_AddDraw.Size = new System.Drawing.Size(71, 13);
            this.lbl_AddDraw.TabIndex = 16;
            this.lbl_AddDraw.Text = "Add Drawing ";
            // 
            // btn_ADDDraw
            // 
            this.btn_ADDDraw.Location = new System.Drawing.Point(7, 56);
            this.btn_ADDDraw.Name = "btn_ADDDraw";
            this.btn_ADDDraw.Size = new System.Drawing.Size(110, 23);
            this.btn_ADDDraw.TabIndex = 16;
            this.btn_ADDDraw.Text = "ADD";
            this.btn_ADDDraw.UseVisualStyleBackColor = true;
            this.btn_ADDDraw.Click += new System.EventHandler(this.btn_ADDDraw_Click);
            // 
            // txt_DrawingName
            // 
            this.txt_DrawingName.Location = new System.Drawing.Point(8, 30);
            this.txt_DrawingName.Name = "txt_DrawingName";
            this.txt_DrawingName.Size = new System.Drawing.Size(109, 20);
            this.txt_DrawingName.TabIndex = 0;
            this.txt_DrawingName.Text = "Enter Drawing Name";
            // 
            // pnl_Select
            // 
            this.pnl_Select.Controls.Add(this.lbl_selDraw);
            this.pnl_Select.Controls.Add(this.chk_All);
            this.pnl_Select.Controls.Add(this.chk_Single);
            this.pnl_Select.Controls.Add(this.lbl_selDrawin);
            this.pnl_Select.Controls.Add(this.lbl_No);
            this.pnl_Select.Controls.Add(this.lbl_Count);
            this.pnl_Select.Controls.Add(this.btn_SelectDrawing);
            this.pnl_Select.Enabled = false;
            this.pnl_Select.Location = new System.Drawing.Point(12, 166);
            this.pnl_Select.Name = "pnl_Select";
            this.pnl_Select.Size = new System.Drawing.Size(125, 159);
            this.pnl_Select.TabIndex = 1;
            this.pnl_Select.Visible = false;
            // 
            // lbl_selDraw
            // 
            this.lbl_selDraw.AutoSize = true;
            this.lbl_selDraw.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_selDraw.Location = new System.Drawing.Point(4, 13);
            this.lbl_selDraw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_selDraw.Name = "lbl_selDraw";
            this.lbl_selDraw.Size = new System.Drawing.Size(84, 13);
            this.lbl_selDraw.TabIndex = 8;
            this.lbl_selDraw.Text = "Select Drawings";
            // 
            // chk_All
            // 
            this.chk_All.AutoSize = true;
            this.chk_All.Location = new System.Drawing.Point(17, 61);
            this.chk_All.Name = "chk_All";
            this.chk_All.Size = new System.Drawing.Size(70, 17);
            this.chk_All.TabIndex = 10;
            this.chk_All.Text = "Select All";
            this.chk_All.UseVisualStyleBackColor = true;
            this.chk_All.CheckedChanged += new System.EventHandler(this.Chk_All_CheckedChanged);
            // 
            // chk_Single
            // 
            this.chk_Single.AutoSize = true;
            this.chk_Single.Location = new System.Drawing.Point(17, 38);
            this.chk_Single.Name = "chk_Single";
            this.chk_Single.Size = new System.Drawing.Size(91, 17);
            this.chk_Single.TabIndex = 11;
            this.chk_Single.Text = "Select Single ";
            this.chk_Single.UseVisualStyleBackColor = true;
            this.chk_Single.CheckedChanged += new System.EventHandler(this.Chk_Single_CheckedChanged);
            // 
            // lbl_selDrawin
            // 
            this.lbl_selDrawin.AutoSize = true;
            this.lbl_selDrawin.Location = new System.Drawing.Point(16, 121);
            this.lbl_selDrawin.Name = "lbl_selDrawin";
            this.lbl_selDrawin.Size = new System.Drawing.Size(91, 13);
            this.lbl_selDrawin.TabIndex = 12;
            this.lbl_selDrawin.Text = "Selected Drawing";
            // 
            // lbl_No
            // 
            this.lbl_No.AutoSize = true;
            this.lbl_No.Location = new System.Drawing.Point(72, 137);
            this.lbl_No.Name = "lbl_No";
            this.lbl_No.Size = new System.Drawing.Size(10, 13);
            this.lbl_No.TabIndex = 15;
            this.lbl_No.Text = "-";
            // 
            // lbl_Count
            // 
            this.lbl_Count.AutoSize = true;
            this.lbl_Count.Location = new System.Drawing.Point(26, 136);
            this.lbl_Count.Name = "lbl_Count";
            this.lbl_Count.Size = new System.Drawing.Size(41, 13);
            this.lbl_Count.TabIndex = 13;
            this.lbl_Count.Text = "Count :";
            // 
            // btn_SelectDrawing
            // 
            this.btn_SelectDrawing.Location = new System.Drawing.Point(13, 89);
            this.btn_SelectDrawing.Name = "btn_SelectDrawing";
            this.btn_SelectDrawing.Size = new System.Drawing.Size(100, 23);
            this.btn_SelectDrawing.TabIndex = 14;
            this.btn_SelectDrawing.Text = "Update";
            this.btn_SelectDrawing.UseVisualStyleBackColor = true;
            this.btn_SelectDrawing.Click += new System.EventHandler(this.Btn_SelectDrawing_Click);
            // 
            // btn_DelDrw
            // 
            this.btn_DelDrw.Location = new System.Drawing.Point(273, 513);
            this.btn_DelDrw.Name = "btn_DelDrw";
            this.btn_DelDrw.Size = new System.Drawing.Size(103, 23);
            this.btn_DelDrw.TabIndex = 19;
            this.btn_DelDrw.Text = "Delete Drawing";
            this.btn_DelDrw.UseVisualStyleBackColor = true;
            this.btn_DelDrw.Click += new System.EventHandler(this.btn_DelDrw_Click);
            // 
            // btn_AddCad
            // 
            this.btn_AddCad.Location = new System.Drawing.Point(164, 513);
            this.btn_AddCad.Name = "btn_AddCad";
            this.btn_AddCad.Size = new System.Drawing.Size(103, 23);
            this.btn_AddCad.TabIndex = 18;
            this.btn_AddCad.Text = "Add Drawing";
            this.btn_AddCad.UseVisualStyleBackColor = true;
            this.btn_AddCad.Click += new System.EventHandler(this.btn_AddCad_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.Location = new System.Drawing.Point(491, 513);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.Size = new System.Drawing.Size(107, 23);
            this.btn_Update.TabIndex = 16;
            this.btn_Update.Text = "Update OpenBrim";
            this.btn_Update.UseVisualStyleBackColor = true;
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // btn_Genrate
            // 
            this.btn_Genrate.Location = new System.Drawing.Point(382, 513);
            this.btn_Genrate.Name = "btn_Genrate";
            this.btn_Genrate.Size = new System.Drawing.Size(103, 23);
            this.btn_Genrate.TabIndex = 1;
            this.btn_Genrate.Text = "Update AutoCAD";
            this.btn_Genrate.UseVisualStyleBackColor = true;
            this.btn_Genrate.Click += new System.EventHandler(this.Btn_Genrate_Click);
            // 
            // pnl_CAD
            // 
            this.pnl_CAD.Location = new System.Drawing.Point(143, 7);
            this.pnl_CAD.Name = "pnl_CAD";
            this.pnl_CAD.Size = new System.Drawing.Size(491, 500);
            this.pnl_CAD.TabIndex = 0;
            // 
            // lbl_Rectangle
            // 
            this.lbl_Rectangle.AutoSize = true;
            this.lbl_Rectangle.Location = new System.Drawing.Point(7, 161);
            this.lbl_Rectangle.Name = "lbl_Rectangle";
            this.lbl_Rectangle.Size = new System.Drawing.Size(56, 13);
            this.lbl_Rectangle.TabIndex = 23;
            this.lbl_Rectangle.Text = "Rectangle";
            // 
            // lbl_RectangleNo
            // 
            this.lbl_RectangleNo.AutoSize = true;
            this.lbl_RectangleNo.Location = new System.Drawing.Point(82, 161);
            this.lbl_RectangleNo.Name = "lbl_RectangleNo";
            this.lbl_RectangleNo.Size = new System.Drawing.Size(10, 13);
            this.lbl_RectangleNo.TabIndex = 24;
            this.lbl_RectangleNo.Text = "-";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 579);
            this.Controls.Add(this.Tab_Control);
            this.Name = "MainForm";
            this.Text = "OpenBrim";
            this.Tab_Control.ResumeLayout(false);
            this.Tab_Project.ResumeLayout(false);
            this.Tab_Project.PerformLayout();
            this.Tab_CAD.ResumeLayout(false);
            this.pnl_SelEntites.ResumeLayout(false);
            this.pnl_SelEntites.PerformLayout();
            this.pnl_del.ResumeLayout(false);
            this.pnl_del.PerformLayout();
            this.pnl_ADD.ResumeLayout(false);
            this.pnl_ADD.PerformLayout();
            this.pnl_Select.ResumeLayout(false);
            this.pnl_Select.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabControl Tab_Control;
        private System.Windows.Forms.TabPage Tab_Project;
        private System.Windows.Forms.ComboBox lst_Author;
        private System.Windows.Forms.ComboBox lst_proj;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Tab_CAD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnl_3D;
        private System.Windows.Forms.ComboBox lst_objects;
        private System.Windows.Forms.Button btn_Genrate;
        private System.Windows.Forms.Panel pnl_CAD;
        private System.Windows.Forms.Label lbl_selDraw;
        private System.Windows.Forms.CheckBox chk_All;
        private System.Windows.Forms.CheckBox chk_Single;
        private System.Windows.Forms.Label lbl_selDrawin;
        private System.Windows.Forms.Label lbl_Count;
        private System.Windows.Forms.Label lbl_No;
        private System.Windows.Forms.Button btn_SelectDrawing;
        private System.Windows.Forms.Button btn_Update;
        private System.Windows.Forms.Button btn_DelDrw;
        private System.Windows.Forms.Button btn_AddCad;
        private System.Windows.Forms.Panel pnl_Select;
        private System.Windows.Forms.Panel pnl_ADD;
        private System.Windows.Forms.TextBox txt_DrawingName;
        private System.Windows.Forms.Button btn_ADDDraw;
        private System.Windows.Forms.Label lbl_AddDraw;
        private System.Windows.Forms.Panel pnl_del;
        private System.Windows.Forms.Label lbl_del;
        private System.Windows.Forms.Button btn_delDrawing;
        private System.Windows.Forms.ComboBox lst_delDraw;
        private System.Windows.Forms.Panel pnl_SelEntites;
        private System.Windows.Forms.Label lbl_SelEnt;
        private System.Windows.Forms.Label lbl_line;
        private System.Windows.Forms.Label lbl_CADShape;
        private System.Windows.Forms.Label lbl_Circles;
        private System.Windows.Forms.Label lbl_CADShapeNo;
        private System.Windows.Forms.Label lbl_CircleNo;
        private System.Windows.Forms.Label lbl_LineNo;
        private System.Windows.Forms.Button btn_EntDrwADD;
        private System.Windows.Forms.ComboBox lst_EntADD;
        private System.Windows.Forms.Label lbl_EllipseNo;
        private System.Windows.Forms.Label lbl_Ellipse;
        private System.Windows.Forms.Label lbl_RectangleNo;
        private System.Windows.Forms.Label lbl_Rectangle;
    }
}

