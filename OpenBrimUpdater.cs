﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using OBrIM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OBrIMACAD
{
    class OpenBrIMUpdater
    {
        internal static void UpdateOBrimCADObject(Form myForm, string CADID)
        {
            double LineCounter = 0;
            double CircleCouner = 0;
            double CADShapeCounter = 0;
            double EllipseCounter = 0;
            double RectangleCounter = 0;
            Document Doc = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument;
            Doc.LockDocument();
            Editor Ed = Autodesk.AutoCAD.ApplicationServices.Application.DocumentManager.MdiActiveDocument.Editor;
            Database DataBase = Ed.Document.Database;
            using (Transaction Trans = DataBase.TransactionManager.StartTransaction())
            {
                PromptSelectionResult SelectPrompt = Doc.Editor.GetSelection();
                if (SelectPrompt.Status == PromptStatus.OK)
                {
                    SelectionSet SelectionSet = SelectPrompt.Value;
                    foreach (SelectedObject SelectedObject in SelectionSet)
                    {
                        Entity SelectedEntity = Trans.GetObject(SelectedObject.ObjectId, OpenMode.ForWrite) as Entity;

                        if (SelectedEntity.GetType() == typeof(Line))
                        {
                            LineCounter++;
                            UpdateOpenBrIMLine(CADID, (SelectedEntity as Line));
                        }
                        if (SelectedEntity.GetType() == typeof(Circle))
                        {
                            CircleCouner++;
                            UpdateOpenBrimCircle(CADID, (SelectedEntity as Circle));
                        }
                        if (SelectedEntity.GetType() == typeof(Polyline))
                        {
                            bool Rectangle = CheckIfRectangle((SelectedEntity as Polyline));
                            if (Rectangle)
                            {
                                RectangleCounter++;
                                UpdateOpenBrimRectangle(CADID, (SelectedEntity as Polyline));
                            }
                            else if (!Rectangle)
                            {
                                CADShapeCounter++;
                                UpdateOpenOBrimCADShape(CADID, (SelectedEntity as Polyline));
                            }
                        }
                        if (SelectedEntity.GetType() == typeof(Ellipse))
                        {
                            EllipseCounter++;
                            UpdateOpenBrimEllipse(CADID, (SelectedEntity as Ellipse));
                        }
                        SetEntityCounter(LineCounter, CircleCouner, CADShapeCounter, EllipseCounter, RectangleCounter,myForm);

                    }
                    Trans.Commit();
                }
            }
        }

        private static void UpdateOpenBrimRectangle(string CADID, Polyline ACADPolyline)
        {
            OBrIMObj Rectangle = MainForm.Connection.ObjectCreate(CADID, "CADDRect");

            Point3d MaxBound = ACADPolyline.Bounds.Value.MaxPoint;
            Point3d MinBound = ACADPolyline.Bounds.Value.MinPoint;

            double DistanceX = MaxBound.X - MinBound.X;
            double DistanceY = MaxBound.Y - MinBound.Y;

            Point3d MidPoint = new Point3d((MinBound.X + DistanceX / 2) , (MinBound.Y + DistanceY / 2), 0);

            Rectangle.SetParamValueNum("X", MidPoint.X);
            Rectangle.SetParamValueNum("Y", MidPoint.Y);
            Rectangle.SetParamValueNum("RZ", 0);

            Rectangle.SetParamValueNum("W", DistanceX);
            Rectangle.SetParamValueNum("H", DistanceY);
        }

        private static bool CheckIfRectangle(Polyline polyline)
        {
            if (polyline.NumberOfVertices == 4 && polyline.Closed == true)
            {
                LineSegment2d L1 = polyline.GetLineSegment2dAt(0);
                LineSegment2d L2 = polyline.GetLineSegment2dAt(1);
                LineSegment2d L3 = polyline.GetLineSegment2dAt(2);
                LineSegment2d L4 = polyline.GetLineSegment2dAt(3);

                if (L1.Length == L3.Length && L2.Length == L4.Length)
                {
                    return true;
                }
            }
            return false;
        }

        private static void UpdateOpenBrimEllipse(string CADID, Ellipse ACADellipse)
        {
            OBrIMObj Ellipse = MainForm.Connection.ObjectCreate(CADID, "CADDEllipse");
            Ellipse.SetParamValueNum("X", ACADellipse.Center.X);
            Ellipse.SetParamValueNum("Y", ACADellipse.Center.Y);
            Ellipse.SetParamValueNum("RZ", ACADellipse.Center.Z);

            if (ACADellipse.MajorAxis.X > 1)
            {
                Ellipse.SetParamValueNum("Radius1", ACADellipse.MajorRadius);
                Ellipse.SetParamValueNum("Radius2", ACADellipse.MinorRadius);
            }
            else if (ACADellipse.MajorAxis.X <= 0)
            {
                Ellipse.SetParamValueNum("Radius1", ACADellipse.MinorRadius);
                Ellipse.SetParamValueNum("Radius2", ACADellipse.MajorRadius);
            }
        }

        private static void UpdateOpenOBrimCADShape(string CADID, Polyline ACADPolyline)
        {
            OBrIMObj CADShape = MainForm.Connection.ObjectCreate(CADID, "CADDShape");
            string ID = CADShape.ID.ToString();

            Point3d MaxBound = ACADPolyline.Bounds.Value.MaxPoint;
            Point3d MinBound = ACADPolyline.Bounds.Value.MinPoint;

            double DistanceX = Math.Abs(MaxBound.X - MinBound.X);
            double DistanceY = Math.Abs(MaxBound.Y - MinBound.Y);

            //Might Be Problematic alot 
            Point3d MidPoint = new Point3d(DistanceX, DistanceY, 0);

            CADShape.SetParamValueNum("X", MidPoint.X);
            CADShape.SetParamValueNum("Y", MidPoint.Y);

            for (int i = 0; i < ACADPolyline.NumberOfVertices; i++)
            {

                OBrIMObj Point = MainForm.Connection.ObjectCreate(ID, "Point");
                Point.SetParamValueNum("X", ACADPolyline.GetPoint2dAt(i).X);
                Point.SetParamValueNum("Y", ACADPolyline.GetPoint2dAt(i).Y);
                CADShape.Objects.Add(Point);
            }
        }
        private static void UpdateOpenBrimCircle(string CADID, Circle ACADCircle)
        {
            OBrIMObj Circle = MainForm.Connection.ObjectCreate(CADID, "CADDCircle");
            Circle.SetParamValueNum("X", ACADCircle.Center.X);
            Circle.SetParamValueNum("Y", ACADCircle.Center.Y);
            Circle.SetParamValueNum("RZ", ACADCircle.Center.Z);
            Circle.SetParamValueNum("Radius", ACADCircle.Radius);
        }
        private static void UpdateOpenBrIMLine(string CADID, Line ACADLine)
        {
            OBrIM.OBrIMObj line = MainForm.Connection.ObjectCreate(CADID, "CADDLine");

            OBrIM.OBrIMObj p1 = MainForm.Connection.ObjectCreate(line.ID, "Point");
            p1.SetParamValueNum("X", ACADLine.StartPoint.X);
            p1.SetParamValueNum("Y", ACADLine.StartPoint.Y);
            p1.SetParamValueNum("Z", ACADLine.StartPoint.Z);
            OBrIM.OBrIMObj p2 = MainForm.Connection.ObjectCreate(line.ID, "Point");
            p2.SetParamValueNum("X", ACADLine.EndPoint.X);
            p2.SetParamValueNum("Y", ACADLine.EndPoint.Y);
            p2.SetParamValueNum("Z", ACADLine.EndPoint.Z);
        }
        private static void SetEntityCounter(double LineCounter, double CircleCouner, double CADShapeCouner, double EllipseCounter,double RectangleCounter ,Form myForm)
        {
            try
            {
                Label LineNo = myForm.Controls.Find("lbl_LineNo", true).FirstOrDefault() as Label;
                LineNo.Text = LineCounter.ToString();
                Label CircleNo = (Label)myForm.Controls.Find("lbl_CircleNo", true).FirstOrDefault();
                CircleNo.Text = CircleCouner.ToString();
                Label CADShapeNo = (Label)myForm.Controls.Find("lbl_CADShapeNo", true).FirstOrDefault();
                CADShapeNo.Text = CADShapeCouner.ToString();
                Label EllipseNo = (Label)myForm.Controls.Find("lbl_EllipseNo", true).FirstOrDefault();
                EllipseNo.Text = EllipseCounter.ToString();
                Label RectangleNo = (Label)myForm.Controls.Find("lbl_RectangleNo", true).FirstOrDefault();
                RectangleNo.Text = RectangleCounter.ToString();
            }
            catch (Exception)
            {
            }
        }
    }
}
