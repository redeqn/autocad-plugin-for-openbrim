﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OBrIM;
using Autodesk.AutoCAD;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.EditorInput;
using System.Windows.Input;
using System.Windows;
using System;

namespace OBrIMACAD
{
    public partial class MainForm : Form
    {
        UIWindow Window;

        public static OBrIMConn Connection;
        private OBrIMObj CurrentProject;
        private OBrIMObj CurrentItem;
        private OBrIMObj CurrentAuthor;

        private OBrIMObjList AuthorList;
        private OBrIMObjList ItemList;

        private OBrIMResponse CurrentDrawing;
        private OBrIMObj CurrentDrawingData;

        List<object> SelectedDrawing = new List<object>();
        List<OBrIMObj> DrawingLists = new List<OBrIMObj>();

        string DrawingName;

        public string DeletedDrawingID { get; private set; }

        public MainForm()
        {
            InitializeComponent();
            Connection = new OBrIMConn(UIMode.EmbeddedControl, true);
            Window = Connection.GetUserControl();
            this.Controls.Add(Window);
            Connection.MessageReceived += Connection_MessageReceived;
        }


        private void Connection_MessageReceived(string eventName, object args)
        {
            switch (eventName)
            {
                case "AppLoaded":
                    // OpenBrIM server has been connected and we are ready for operation. No operation can be performed before we receive this message.
                    break;
                case "AppLoadStart":
                    break;
                case "ProjectList":         // received the list of projects of the logged in user as response to OpenBrIM.ProjectGetAll() call
                    List<OBrIMResponseProject> userProjects = ((OBrIMResponse)args).Projects;
                    LoadProjectList(userProjects);
                    lst_proj.SelectedIndex = 0;
                    lst_proj.SelectedItem = lst_proj.Items[0];
                    //View3d();
                    break;
                case "UserLoginSuccess":    // user logged in successfully
                    // let's give a second or two for the library objects to be populated. 
                    // library obejcts are loaded into the app asynchronously
                    Timer t = new Timer();
                    t.Tick += delegate (object sender, EventArgs e)
                    {
                        t.Stop();
                        Connection.ProjectGetAll();
                    };
                    t.Interval = 3000;
                    t.Start();
                    break;
                case "CADDSelected":
                    Dictionary<string, object> Json = ((OBrIMResponse)args).JSON;
                    AddCadToList(Json);
                    break;
                case "ProjectCompileComplete": // project comilation is done, we can access project data now.
                    CurrentProject = Connection.ObjectGet();
                    break;
            }
        }

        private void AddCadToList(Dictionary<string, object> Json)
        {
            object CADID;
            Json.TryGetValue("cadd", out CADID);
            CurrentDrawing = Connection.CADDGet(CADID.ToString());
            CurrentDrawingData = CurrentDrawing.ObjData;
            SelectedDrawing.Clear();
            SelectedDrawing.Add(CurrentDrawingData);
            lbl_No.Text = SelectedDrawing.Count().ToString();
        }
        private void LoadProjectList(List<OBrIMResponseProject> userProjects)
        {
            foreach (var Project in userProjects)
            {
                lst_proj.Items.Add(new ListItem(Project.Name, Project.ID));
                lst_proj.DisplayMember = Project.Name;
                lst_proj.ValueMember = Project.ID;
            }
        }

        private void PopulateAuthorList()
        {
            for (int i = 0; i < AuthorList.Count; i++)
            {
                CurrentAuthor = AuthorList.Get(i);
                ListItem L = new ListItem(CurrentAuthor.Name, CurrentAuthor.ID);
                lst_Author.Items.Add(L);
                lst_Author.DisplayMember = CurrentAuthor.Name;
                lst_Author.ValueMember = CurrentAuthor.ID;
            }
            lst_Author.SelectedIndex = 0;
            lst_Author.SelectedItem = lst_Author.Items[0];
        }
        private void PopulateItemList()
        {
            try
            {
                for (int i = 0; i < ItemList.Count; i++)
                {
                    CurrentItem = ItemList.Get(i);
                    ListItem L = new ListItem(CurrentItem.Name, CurrentItem.ID);
                    lst_objects.Items.Add(L);
                    lst_objects.DisplayMember = CurrentItem.Name;
                    lst_objects.ValueMember = CurrentItem.ID;
                }
                lst_objects.SelectedIndex = 0;
                lst_objects.SelectedItem = lst_objects.Items[0];
            }
            catch (System.Exception)
            {
            }
        }
       
        private void View3d()
        {
            try
            {
                Connection.AppShow(AppViews.Graphics3D);
                pnl_3D.Controls.Add(Connection.GetUserControl());
            }
            catch (System.Exception)
            {
            }
        }
        private void GenrateParameters()
        {
            CurrentItem = ItemList.GetByID((lst_objects.SelectedItem as ListItem).Value);
            for (int i = 0; i < CurrentItem.Params.Count(); i++)
            {
            }
        }

        private void LoadCadList()
        {
            DrawingLists.Clear();
            OBrIM.OBrIMObj CurrentProject = MainForm.Connection.ObjectGet();
            for (int i = 0; i < CurrentProject.Objects.Count; i++)
            {
                OBrIMObj Drawing = CurrentProject.Objects.Get(i);
                if (Drawing.ObjType == "CADD")
                {
                    DrawingLists.Add(Drawing);
                }
            }
        }
        private void LoadSelectedDrawing()
        {
            SelectedDrawing.Clear();
            for (int i = 0; i < DrawingLists.Count; i++)
            {
                OBrIMResponse CurrentDrawing = Connection.CADDGet(DrawingLists[i].Name);
                CurrentDrawingData = CurrentDrawing.ObjData;
                SelectedDrawing.Add(CurrentDrawingData);
            }
            lbl_No.Text = SelectedDrawing.Count().ToString();
        }



        //Button Clicks Event
        private void Lst_objects_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenrateParameters();
        }
        private void Lst_Authors_SelectedIndexChanged(object sender, EventArgs e)
        {
            CurrentAuthor = AuthorList.GetByID((lst_Author.SelectedItem as ListItem).Value);
            ItemList = CurrentAuthor.Objects;
            lst_objects.Items.Clear();
            PopulateItemList();
        }
        private void Lst_proj_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ListItem item = (ListItem)lst_proj.SelectedItem;
                string prjID = item.Value;
                AuthorList = Connection.LibObjList().ObjData.Objects;
                Connection.ProjectOpen(prjID);
                lst_Author.Items.Clear();
                PopulateAuthorList();
                View3d();
            }
            catch (System.Exception)
            {
            }
        }
        private void Tab_Objects_TabIndexChanged(object sender, EventArgs e)
        {
            if (Tab_Control.SelectedTab == Tab_Project)
            {
                pnl_CAD.Controls.Remove(Window);
                Connection.AppShow(AppViews.Graphics3D);
                pnl_3D.Controls.Add(Window);
            }
            else if (Tab_Control.SelectedTab == Tab_CAD)
            {
                pnl_3D.Controls.Remove(Window);
                Connection.AppShow(AppViews.CADD);
                pnl_CAD.Controls.Add(Window);
            }
        }
        private void Tab_Objects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Tab_Control.SelectedTab == Tab_Project)
            {
                try
                {
                    pnl_CAD.Controls.Remove(Window);
                    Connection.AppShow(AppViews.Graphics3D);
                    pnl_3D.Controls.Add(Window);
                }
                catch (System.Exception)
                {
                }
            }
            else if (Tab_Control.SelectedTab == Tab_CAD)
            {
                try
                {
                    pnl_3D.Controls.Remove(Window);
                    Connection.AppShow(AppViews.CADD);
                    pnl_CAD.Controls.Add(Window);
                }
                catch (System.Exception)
                {
                }
            }
        }
        private void Chk_Single_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_All.Checked && chk_Single.Checked)
            {
                MessageBox.Show("You Have All Checked");
                return;
            }
            if (chk_Single.Checked)
            {
                OBrIMResponse CADDocument = Connection.CADDUserSelect();
            }
        }
        private void Chk_All_CheckedChanged(object sender, EventArgs e)
        {
            if (chk_Single.Checked && chk_All.Checked)
            {
                MessageBox.Show("You Have Single Checked");
                return;
            }
            if (chk_All.Checked)
            {
                LoadCadList();
                LoadSelectedDrawing();
            }
        }
        private void Btn_SelectDrawing_Click(object sender, EventArgs e)
        {
            if (chk_Single.Checked && chk_All.Checked)
            {
                MessageBox.Show("Uncheck One Option");
                return;
            }
            foreach (OBrIMObj Drawing in SelectedDrawing)
            {
                OBrIMObjList Objects = Drawing.Objects;
                for (int i = 0; i < Objects.Count; i++)
                {
                    OBrIMObj Object = Objects.Get(i);
                    AutoCadDrafter.DrawCad(Object);
                }
            }
        }
        private void Btn_Genrate_Click(object sender, EventArgs e)
        {
            pnl_Select.Enabled = true;
            pnl_Select.Visible = true;
        }
        private void btn_Update_Click(object sender, EventArgs e)
        {
            pnl_SelEntites.Enabled = true;
            pnl_SelEntites.Visible = true;
            LoadCadList();
            lst_EntADD.DataSource = DrawingLists;
            lst_EntADD.SelectedIndex = 0;
            lst_EntADD.SelectedItem = DrawingLists[0];
        }
        private void btn_AddCad_Click(object sender, EventArgs e)
        {
            pnl_ADD.Visible = true;
            pnl_ADD.Enabled = true;
        }
        private void btn_ADDDraw_Click(object sender, EventArgs e)
        {
            DrawingName = txt_DrawingName.Text;
            OBrIM.OBrIMObj prj = Connection.ObjectGet();
            OBrIM.OBrIMObj drawing = Connection.ObjectCreate(prj.ID, "CADD", DrawingName);
            Window.Refresh();
        }
        private void btn_DelDrw_Click(object sender, EventArgs e)
        {
            pnl_del.Enabled = true;
            pnl_del.Visible = true;
            LoadCadList();
            lst_delDraw.DataSource = DrawingLists;
            lst_delDraw.SelectedIndex = 0;
            lst_delDraw.SelectedItem = DrawingLists[0];

        }
        private void btn_delDrawing_Click(object sender, EventArgs e)
        {
            Connection.ObjectDelete(DeletedDrawingID);
            DrawingLists.Clear();
            LoadCadList();
            lst_delDraw.DataSource = DrawingLists;
        }
        private void lbl_LineNo_Click(object sender, EventArgs e)
        {
        }
        private void lst_delDraw_SelectedIndexChanged(object sender, EventArgs e)
        {
            DeletedDrawingID = (lst_delDraw.SelectedItem as OBrIMObj).ID.ToString();
        }
        private void btn_EntDrwADD_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Select Entites", "OpenBrIM");
            string CADID = (lst_EntADD.SelectedValue as OBrIMObj).ID;
            OpenBrIMUpdater.UpdateOBrimCADObject(this, CADID);
        }
        private void lst_DrawLstAdd_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        #region
        //WORKINGGGGGGGGGG
        //private void LoadCadList()
        //{
        //    OBrIMResponse CADlist = Connection.CADDList();
        //    Dictionary<string, object> Json = CADlist.JSON;

        //    object[] JsonCadList = null;
        //    foreach (var item in Json)
        //    {
        //        if (item.Key == "cadds")
        //        {
        //            JsonCadList = item.Value as object[];
        //        }
        //    }
        //    for (int i = 0; i < JsonCadList.Count(); i++)
        //    {
        //        var CADID = (JsonCadList[i] as Dictionary<string, object>).ElementAt(0).Value;
        //        DrawingLists.Add(CADID.ToString());
        //    }
        //}
        #endregion
    }
}
