﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OBrIMACAD
{
    class Utils
    {
        public static string ApplicationPath
        {
            get
            {
                string dllFile = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                return System.IO.Path.GetDirectoryName(dllFile);
            }
        }

    }
}
