# OpenBrIM Plugin for AutoCAD #

This is a plugin library allowing AutoCAD to interact with OpenBrIM servers. 

### Compiling and Debugging ###

* Checkout a copy of the plugin source code on your computer using a Git client. The URL is: 
https://bitbucket.org/redeqn/autocad-plugin-for-openbrim.git
* Open up OBrIMACAD solution file in Visual Studio (we use version 2015)
* In Visual Studio, from the menu, go to Project -> OBrIMACAD Properties -> Debug -> Start External Program and add your AutoCAD path. 
* Start AutoCAD and execute command netload. Select the output dll name (OBrIMACAD.dll)
* You should be able to see a new ribbon named OpenBrIM and a single button wait for everything to load.

